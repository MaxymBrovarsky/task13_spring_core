package com.brom.epam.task13.model;

public interface BeanValidator {
  boolean validate();
}
