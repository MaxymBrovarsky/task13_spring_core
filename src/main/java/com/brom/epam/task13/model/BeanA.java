package com.brom.epam.task13.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class BeanA implements BeanValidator, InitializingBean, DisposableBean {
  private static Logger logger = LogManager.getLogger(BeanA.class.getName());
  private String name = "";
  private int value = 0;

  @Override
  public String toString() {
    return "BeanA{" +
        "name='" + name + '\'' +
        ", value=" + value +
        '}';
  }

  public String getName() {
    return name;
  }

  public int getValue() {
    return value;
  }

  public boolean validate() {
    if (name.isEmpty() || value < 0) {
      return false;
    }
    return true;
  }

  @Override
  public void destroy() throws Exception {
    logger.info(this.getClass().getName() + "destroy from DisposableBean");
  }

  @Override
  public void afterPropertiesSet() throws Exception {
    logger.info(this.getClass().getName() + "afterPropertiesSet from InitializingBean");
  }
}
