package com.brom.epam.task13.model;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class BeanF implements Validator, BeanValidator {
  private String name;
  private int value;

  public BeanF(String name, int value) {
    this.name = name;
    this.value = value;
  }

  @Override
  public String toString() {
    return "BeanF{" +
        "name='" + name + '\'' +
        ", value=" + value +
        '}';
  }

  public boolean validate() {
    if (name.matches("[^(epam)]")) {
      return false;
    }
    return true;
  }

  public boolean supports(Class<?> aClass) {
    return false;
  }

  public void validate(Object o, Errors errors) {

  }
}
