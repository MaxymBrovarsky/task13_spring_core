package com.brom.epam.task13.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@PropertySource("values.properties")
public class BeanB implements Validator, BeanValidator {
  private static Logger logger = LogManager.getLogger(BeanB.class.getName());
  private String name;
  private int value;

  public BeanB(String name, int value) {
    this.name = name;
    this.value = value;
  }

  @Override
  public String toString() {
    return "BeanB{" +
        "name='" + name + '\'' +
        ", value=" + value +
        '}';
  }

  public void init() {
    logger.info(this.getClass().getName() + " init");
  }

  public void anotherInit() {
    logger.info(this.getClass().getName() + " init");
  }

  public void destroy() {
    logger.info(this.getClass().getName() + " destroy");
  }

  public boolean validate() {
    if (name.length() < 3 || value == 0) {
      return false;
    }
    return true;
  }

  public boolean supports(Class<?> aClass) {
    return false;
  }

  public void validate(Object o, Errors errors) {

  }
}
