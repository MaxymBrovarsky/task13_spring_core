package com.brom.epam.task13.configuration;

import com.brom.epam.task13.model.BeanA;
import com.brom.epam.task13.model.BeanB;
import com.brom.epam.task13.model.BeanC;
import com.brom.epam.task13.model.BeanD;
import com.brom.epam.task13.model.BeanE;
import com.brom.epam.task13.model.BeanF;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.annotation.Order;

@Configuration
@PropertySource("values.properties")
public class ConfigurationA {
  @Value("${beanb.name}")
  private String beanBName;
  @Value("${beanb.value}")
  private int beanBValue;
  @Value("${beanc.name}")
  private String beanCName;
  @Value("${beanc.value}")
  private int beanCValue;
  @Value("${beand.name}")
  private String beanDName;
  @Value("${beand.value}")
  private int beanDValue;

  @Bean
  public BeanA getBeanAFirst(BeanB beanB, BeanC beanC) {
    return new BeanA();
  }
  @Bean("beanASecond")
  public BeanA getBeanASecond(BeanB beanB, BeanD beanD) {
    return new BeanA();
  }
  @Bean("beanAThird")
  public BeanA getBeanAThird(BeanD beanD, BeanC beanC) {
    return new BeanA();
  }

  @Bean(initMethod = "init", destroyMethod = "destroy")
  @Order(2)
  public BeanB getBeanB() {
    return new BeanB(beanBName, beanBValue);
  }
  @Bean(initMethod = "init", destroyMethod = "destroy")
  @Order(3)
  public BeanC getBeanC() {
    return new BeanC(beanCName, beanCValue);
  }
  @Bean(initMethod = "init", destroyMethod = "destroy")
  @Order(1)
  public BeanD getBeanD() {
    return new BeanD(beanDName, beanDValue);
  }
  @Bean
  public BeanE getBeanEFirst(BeanA getBeanAFirst) {
    return new BeanE(getBeanAFirst.getName(), getBeanAFirst.getValue());
  }
  @Bean
  public BeanE getBeanESecond(@Qualifier("beanASecond") BeanA beanA) {
    return new BeanE(beanA.getName(), beanA.getValue());
  }
  @Bean("beanEThird")
  public BeanE getBeanEThird(@Qualifier("beanAThird") BeanA beanA) {
    return new BeanE(beanA.getName(), beanA.getValue());
  }
  @Bean
  @Lazy
  public BeanF getBeanF() {
    return new BeanF("qqq", 1);
  }
}
