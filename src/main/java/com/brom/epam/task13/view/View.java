package com.brom.epam.task13.view;

import com.brom.epam.task13.controller.Controller;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class View {
  private Logger logger = LogManager.getLogger(View.class.getName());
  private Scanner input = new Scanner(System.in);
  private Map<String, String> menu;
  private Map<String, Command> menuCommands;
  private Controller controller;

  public View() {
    this.initMenu();
    this.initMenuCommands();
    this.controller = new Controller();
  }

  private void initMenu() {
    this.menu = new HashMap<>();
    this.menuCommands = new HashMap<>();
  }

  private void initMenuCommands() {
    this.putMenuCommand(
        "1",
        "Show all beans loaded by ConsigurationB",
        this::showAllBeansLoadedByConfigurationA
    );
    this.putMenuCommand(
        "2",
        "Show configuration of all beans",
        this::showConfigurationOfAllBeans
    );
  }

  private void putMenuCommand(String key, String description, Command command) {
    this.menu.put(key, key+"."+description);
    this.menuCommands.put(key, command);
  }

  private void showAllBeansLoadedByConfigurationA() {
    logger.info(controller.getAllBeansCreatedByConfigurationB());
  }
  private void showConfigurationOfAllBeans() {
    logger.info(controller.getConfigurationOfAllBeans());
  }

  public void show() {
    while (true) {
      this.menu.values().forEach(v -> logger.info(v));
      try {
        String key = input.nextLine();
        menuCommands.get(key).execute();
      } catch (NullPointerException e) {
        logger.error("no such command");
      }
    }
  }

}
