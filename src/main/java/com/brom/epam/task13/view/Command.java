package com.brom.epam.task13.view;

public interface Command {
  void execute();
}
