package com.brom.epam.task13.controller;

import com.brom.epam.task13.configuration.ConfigurationB;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Controller {
  public String getConfigurationOfAllBeans() {
    AnnotationConfigApplicationContext context =
        new AnnotationConfigApplicationContext(ConfigurationB.class);
    StringBuilder stringBuilder = new StringBuilder();
    for(String name: context.getBeanDefinitionNames()) {
      BeanDefinition beanDefinition = context.getBeanDefinition(name);
      stringBuilder.append(beanDefinition + " \n");
    }
    return stringBuilder.toString();
  }

  public String getAllBeansCreatedByConfigurationB() {
    AnnotationConfigApplicationContext context =
        new AnnotationConfigApplicationContext(ConfigurationB.class);
    StringBuilder stringBuilder = new StringBuilder();
    for (String beanDefinitionName : context.getBeanDefinitionNames()) {
      stringBuilder.append(beanDefinitionName + " \n");
    }
    return stringBuilder.toString();
  }
}
