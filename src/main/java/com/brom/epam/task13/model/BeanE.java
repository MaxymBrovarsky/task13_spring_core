package com.brom.epam.task13.model;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class BeanE implements Validator, BeanValidator {
  private static Logger logger = LogManager.getLogger(BeanA.class.getName());
  private String name;
  private int value;

  public BeanE(String name, int value) {
    this.name = name;
    this.value = value;
  }

  @Override
  public String toString() {
    return "BeanE{" +
        "name='" + name + '\'' +
        ", value=" + value +
        '}';
  }

  public boolean supports(Class<?> aClass) {
    return false;
  }

  public void validate(Object o, Errors errors) {

  }

  public boolean validate() {
    if (name.isEmpty()) {
      return false;
    }
    return true;
  }

  @PostConstruct
  public void init() {
    logger.info(this.getClass().getName() + " init");
  }

  @PreDestroy
  public void destroy() {
    logger.info(this.getClass().getName() + " destroy");
  }
}
