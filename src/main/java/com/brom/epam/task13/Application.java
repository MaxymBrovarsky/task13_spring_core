package com.brom.epam.task13;

import com.brom.epam.task13.view.View;

public class Application {

  public static void main(String[] args) {
    new View().show();
  }
}
