package com.brom.epam.task13.configuration;

import com.brom.epam.task13.model.MyBeanFactoryPostProcessor;
import com.brom.epam.task13.model.MyBeanPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(ConfigurationA.class)
public class ConfigurationB {
  @Bean
  public MyBeanFactoryPostProcessor myBeanFactoryPostProcessor() {
    return new MyBeanFactoryPostProcessor();
  }
  @Bean
  public MyBeanPostProcessor myBeanPostProcessor() {
    return new MyBeanPostProcessor();
  }
}
