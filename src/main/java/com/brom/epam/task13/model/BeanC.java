package com.brom.epam.task13.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class BeanC implements Validator, BeanValidator {
  private static Logger logger = LogManager.getLogger(BeanC.class.getName());
  private String name;
  private int value;

  public BeanC(String name, int value) {
    this.name = name;
    this.value = value;
  }

  public void init() {
    logger.info(this.getClass().getName() + " init");
  }

  public void destroy() {
    logger.info(this.getClass().getName() + " destroy");
  }
  @Override
  public String toString() {
    return "BeanC{" +
        "name='" + name + '\'' +
        ", value=" + value +
        '}';
  }

  public boolean validate() {
    if (name.equalsIgnoreCase("SoftServe")) {
      return false;
    }
    return true;
  }

  public boolean supports(Class<?> aClass) {
    return false;
  }

  public void validate(Object o, Errors errors) {

  }
}
